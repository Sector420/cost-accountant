package hu.bme.aut.android.costaccountant.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.data.Cost
import hu.bme.aut.android.costaccountant.databinding.DialogCreateCostBinding
import java.util.*

class CostDialogFragment(amount: Double) : DialogFragment() {

    interface CostHandler {
        fun costCreated(cost: Cost, amount : String)
    }

    private lateinit var costHandler: CostHandler

    private lateinit var etName: EditText
    private lateinit var etPrice: EditText
    private lateinit var etDate: DatePicker
    private lateinit var category: Spinner

    private lateinit var binding: DialogCreateCostBinding

    private val max = amount

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is CostHandler) {
            throw RuntimeException("The Activity does not implement the CostHandler interface")
        }
        costHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogCreateCostBinding.inflate(LayoutInflater.from(context))
        etName = binding.root.findViewById<EditText>(R.id.etName)
        etPrice = binding.root.findViewById<EditText>(R.id.etPrice)
        etDate = binding.root.findViewById<DatePicker>(R.id.etDate)
        category = binding.root.findViewById<Spinner>(R.id.Category)
        category.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.category_items)
        )
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle("Add cost")
                .setView(binding.root)
                .setPositiveButton("Create"){ _, _ -> null }
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (isValid()) {
                if (max-etPrice.text.toString().toDouble() < 0) {
                    etPrice.error="Not enough money"
                }
                else {
                    handleCostCreate(etPrice.text.toString())
                    dialog.dismiss()
                }
            }
        }
    }

    private fun isValid(): Boolean {
        val validName = etName.text.isNotEmpty()
        if (!validName) {
            etName.error = "Name can not be empty"
        }
        val validPrice = etPrice.text.isNotEmpty()
        if (!validPrice) {
            etPrice.error = "Price can not be empty"
        }
        return validName && validPrice
    }


    private fun handleCostCreate(amount : String) {
        costHandler.costCreated(
            Cost(
                etName.text.toString(),
                etPrice.text.toString().toDouble(),
                Cost.Category.getByOrdinal(category.selectedItemPosition)
                    ?: Cost.Category.ANOTHER,
                getDateFrom(etDate)
            ) , amount
        )
    }

    private fun getDateFrom(picker: DatePicker): String? {
        return String.format(
            Locale.getDefault(), "%04d.%02d.%02d.",
            picker.year, picker.month + 1, picker.dayOfMonth
        )
    }
}