package hu.bme.aut.android.costaccountant.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.DatePicker
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.databinding.DialogCreateDateListBinding
import java.util.*

class DateListDialogFragment : DialogFragment()  {

    interface DateListHandler {
        fun createDateList(name : String, price : String, start : String, end : String)
    }

    private lateinit var dateListHandler: DateListHandler

    private lateinit var etName: EditText
    private lateinit var etPriceLimit: EditText
    private lateinit var etStartDate: DatePicker
    private lateinit var etEndDate: DatePicker

    private lateinit var binding: DialogCreateDateListBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is DateListHandler) {
            throw RuntimeException("The Activity does not implement the DateListHandler interface")
        }
        dateListHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogCreateDateListBinding.inflate(LayoutInflater.from(context))
        etName = binding.root.findViewById<EditText>(R.id.etName)
        etPriceLimit = binding.root.findViewById<EditText>(R.id.etPrice_limit)
        etStartDate = binding.root.findViewById<DatePicker>(R.id.etStartDate)
        etEndDate = binding.root.findViewById<DatePicker>(R.id.etEndDate)
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle("Add date")
                .setView(binding.root)
                .setPositiveButton("Create"){ _, _ -> null }
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (isValid()) {
                handleDateListCreate()
                dialog.dismiss()
            }
        }
    }

    private fun getDateFrom(picker: DatePicker): String {
        return String.format(
            Locale.getDefault(), "%04d.%02d.%02d.",
            picker.year, picker.month + 1, picker.dayOfMonth
        )
    }

    private fun isValid(): Boolean {
        val validName = etName.text.isNotEmpty()
        if (!validName) {
            etName.error = "Name can not be empty"
        }
        val validPrice = etPriceLimit.text.isNotEmpty()
        if (!validPrice) {
            etPriceLimit.error = "Price can not be empty"
        }
        return validName && validPrice
    }

    private fun handleDateListCreate() {
        dateListHandler.createDateList(etName.text.toString(),etPriceLimit.text.toString(),getDateFrom(etStartDate),getDateFrom(etEndDate))
    }

}