package hu.bme.aut.android.costaccountant.adapter

import android.annotation.SuppressLint
import android.content.Context

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.costaccountant.activity.MainActivity
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.data.DateList
import hu.bme.aut.android.costaccountant.databinding.ItemDateListBinding


class DateListsAdapter (private val context: Context) :
    ListAdapter<DateList, DateListsAdapter.DateListViewHolder>(itemCallback) {

    private val dateListList: MutableList<DateList> = mutableListOf()
    private val idMap: MutableMap<DateList, String> = mutableMapOf()

    class DateListViewHolder(binding: ItemDateListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val tvName: TextView = binding.tvName
        val tvDate: TextView = binding.tvDate
        val ivIcon: ImageView = binding.ivIcon
        val deleteButton = binding.RemoveButton
        val shareButton = binding.ShareButton
        val root = binding.root
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DateListViewHolder(
            ItemDateListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DateListViewHolder, position: Int) {
        val tmpDateList = dateListList[position]
        holder.tvName.text = tmpDateList.name
        holder.tvDate.text = "${tmpDateList.startDate} - ${tmpDateList.endDate}"
        holder.ivIcon.setImageResource(R.drawable.trip_icon)
        holder.root.setOnClickListener {
            (context as MainActivity).startCostsActivity(idMap[tmpDateList]!!, tmpDateList.priceLimit, tmpDateList.deleted, tmpDateList.stuffname!!)
        }
        holder.deleteButton.setOnClickListener{
            (context as MainActivity).deleteDateList(idMap[tmpDateList]!!)
        }
        holder.shareButton.setOnClickListener{
            (context as MainActivity).shareDateList(idMap[tmpDateList]!!)
        }
    }

    fun addDate(newDateList: DateList?, id: String) {
        newDateList ?: return
        idMap[newDateList] = id
        dateListList += newDateList
        submitList(dateListList)
        notifyItemInserted(dateListList.lastIndex)
    }

    fun updateDateList(modifiedDateList: DateList?, dateListId: String) {
        modifiedDateList ?: return
        val index = dateListList.indexOfFirst { dateList: DateList ->
            idMap[dateList].equals(dateListId)
        }
        idMap.remove(dateListList[index])
        dateListList[index] = modifiedDateList
        idMap[dateListList[index]] = dateListId
        submitList(dateListList)
        notifyItemChanged(index)
    }

    fun removeDate(id: String) {
        val index = dateListList.indexOfFirst { dateList: DateList ->
            idMap[dateList].equals(id)
        }
        idMap.remove(dateListList[index])
        dateListList.removeAt(index)
        submitList(dateListList)
        notifyItemRemoved(index)
    }

    companion object {

        object itemCallback : DiffUtil.ItemCallback<DateList>() {
            override fun areItemsTheSame(
                oldDateList: DateList,
                newDateList: DateList
            ): Boolean {
                return oldDateList == newDateList
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(
                oldDateList: DateList,
                newDateList: DateList
            ): Boolean {
                return oldDateList == newDateList
            }
        }
    }
}
