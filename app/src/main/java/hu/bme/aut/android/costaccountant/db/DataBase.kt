package hu.bme.aut.android.costaccountant.db

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import hu.bme.aut.android.costaccountant.data.Cost
import hu.bme.aut.android.costaccountant.data.DateList

class DataBase {
    companion object {
        private const val TAG = "DataBase"

        private const val DATE_LISTS = "lists"
        private const val COSTS = "costs"
        private const val THEBOYS = "theboys"
        private const val USER_UID = "userUid"
        private const val DATE_LIST_ID = "dateListId"
        private const val JOIN_REQUESTS = "joinRequests"

        private var instance = DataBase()

        fun getInstance(): DataBase {
            return instance
        }
    }

    private var db = Firebase.firestore

    fun createDateList(dateListToCreate: DateList) {
        db.collection(DATE_LISTS)
            .add(dateListToCreate.toHashMap())
    }

    fun deleteDateList(dateListId: String) {
        db.collection(DATE_LISTS).document(dateListId).delete()
    }

    private fun updateDateList(dateListId: String, price: String?, deleted: String, name: String) {
        db.collection(DATE_LISTS).document(dateListId).update("priceLimit", price)
        db.collection(DATE_LISTS).document(dateListId).update("deleted", deleted)
        db.collection(DATE_LISTS).document(dateListId).update("stuffname", name)
    }

    fun addDateListsListener(listener: EventListener<QuerySnapshot?>) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(DATE_LISTS).whereArrayContains(THEBOYS, user.uid)
            .addSnapshotListener(listener)
    }

    fun createCost(
        dateListId: String,
        costToCreate: Cost,
        price: String,
        deleted: String,
        name: String
    ) {
        db.collection(DATE_LISTS).document(dateListId).collection(COSTS)
            .add(costToCreate.toHashMap())
        updateDateList(dateListId, price, deleted, name)
    }

    fun deleteCost(dateListId: String, costId: String, price: String, deleted: String, name: String) {
        db.collection(DATE_LISTS).document(dateListId).collection(COSTS)
            .document(costId)
            .delete()
        updateDateList(dateListId, price, deleted, name)
    }

    fun addCostListener(dateListId: String, listener: EventListener<QuerySnapshot?>) {
        db.collection(DATE_LISTS).document(dateListId).collection(COSTS)
            .addSnapshotListener(listener)
    }

    fun joinDateList(dateListId: String) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        val joinRequest = hashMapOf<String, Any>(
            USER_UID to user.uid,
            DATE_LIST_ID to dateListId
        )
        db.collection(JOIN_REQUESTS)
            .add(joinRequest)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "Join request added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error adding join request", error)
            }
    }
}