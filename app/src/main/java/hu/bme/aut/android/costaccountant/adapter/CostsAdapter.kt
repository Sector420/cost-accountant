package hu.bme.aut.android.costaccountant.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.costaccountant.activity.CostsActivity
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.data.Cost
import hu.bme.aut.android.costaccountant.databinding.CostItemBinding


class CostsAdapter (private val context: Context) :
    ListAdapter<Cost, CostsAdapter.CostViewHolder>(itemCallback) {

    private val costList: MutableList<Cost> = mutableListOf()
    private val costIdMap: MutableMap<Cost, String> = mutableMapOf()

    class CostViewHolder(binding: CostItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val tvName = binding.tvName
        val tvPrice = binding.tvPrice
        val ivIcon: ImageView = binding.ivIcon
        val deleteButton = binding.RemoveButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CostViewHolder(
            CostItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CostViewHolder, position: Int) {
        val tmpCost = costList[position]
        holder.tvName.text = "${tmpCost.name}  (${tmpCost.date})"
        holder.tvPrice.text = tmpCost.price.toString()
        holder.ivIcon.setImageResource(getImageResource(tmpCost.category))
        holder.deleteButton.setOnClickListener{
            var temp = costList.get(0)
            costList.forEach {
                if (temp.price!! > it.price!!) {
                    temp = it
                }
            }
            costList.forEach {
                if(temp.price!! < it.price!! && it.price!! < tmpCost.price!!) {
                    temp = it
                }
            }
            (context as CostsActivity).deleteCost(costIdMap[tmpCost]!!, tmpCost.price, temp, costList.size)
        }
    }

    @DrawableRes
    private fun getImageResource(category: Cost.Category?) = when (category) {
        Cost.Category.DIESEL -> R.drawable.diesel
        Cost.Category.FOOD -> R.drawable.food
        Cost.Category.HOTEL -> R.drawable.hotel
        Cost.Category.MEDICINE -> R.drawable.medicine
        else -> R.drawable.question
    }

    fun addCost(newCost: Cost?, id: String) {
        newCost ?: return
        costIdMap[newCost] = id
        costList += newCost
        submitList(costList)
        notifyItemInserted(costList.lastIndex)
    }

    fun removeCost(id: String) {
        var index = costList.indexOfFirst { cost: Cost ->
            costIdMap[cost].equals(id)
        }
        costIdMap.remove(costList[index])
        costList.removeAt(index)
        submitList(costList)
        notifyItemRemoved(index)
    }

    companion object {

        private const val TAG = "CostsAdapter"

        object itemCallback : DiffUtil.ItemCallback<Cost>() {
            override fun areItemsTheSame(oldCost: Cost, newCost: Cost): Boolean {
                return oldCost == newCost
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldCost: Cost, newCost: Cost): Boolean {
                return oldCost == newCost
            }
        }
    }
}