package hu.bme.aut.android.costaccountant.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.*
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.databinding.DialogInfoBinding

class InfoDialogFragment(private var deleted: Double, private var name: String) : DialogFragment() {

    private lateinit var etMoney: TextView
    private lateinit var etMost: TextView

    private lateinit var binding: DialogInfoBinding


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogInfoBinding.inflate(LayoutInflater.from(context))
        etMoney = binding.root.findViewById<TextView>(R.id.etMoney)
        etMost = binding.root.findViewById<TextView>(R.id.etMost)
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle("Just info")
                .setView(binding.root)
                .setPositiveButton("Ok"){ _, _ -> null }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()
        etMoney.text = deleted.toString()
        etMost.text = name
        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            dialog.dismiss()
        }
    }

}