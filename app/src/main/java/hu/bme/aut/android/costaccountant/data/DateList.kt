package hu.bme.aut.android.costaccountant.data

import java.io.Serializable

class DateList (
    var name: String? = null,
    var priceLimit: String? = null,
    var deleted: String? = null,
    var startDate: String? = null,
    var endDate: String? = null,
    var stuffname: String? = null,
    var theboys: List<String>? = null
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "priceLimit" to priceLimit,
            "deleted" to deleted,
            "startDate" to startDate,
            "endDate" to endDate,
            "stuffname" to stuffname,
            "theboys" to theboys
        )
    }
}