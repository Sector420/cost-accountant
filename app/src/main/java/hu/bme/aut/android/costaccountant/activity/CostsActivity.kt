package hu.bme.aut.android.costaccountant.activity


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.adapter.CostsAdapter
import hu.bme.aut.android.costaccountant.data.Cost
import hu.bme.aut.android.costaccountant.databinding.ActivityCostsBinding
import hu.bme.aut.android.costaccountant.db.DataBase
import hu.bme.aut.android.costaccountant.fragment.CostDialogFragment
import hu.bme.aut.android.costaccountant.fragment.InfoDialogFragment

class CostsActivity : AppCompatActivity(), CostDialogFragment.CostHandler {

    companion object {
        private const val TAG = "CostsActivity"

        fun createIntent(
            context: Context,
            id: String,
            priceLimit: String?,
            deleted: String?,
            stuffname: String
        ): Intent {
            val extras = Bundle()
            extras.putString("LIST_ID", id)
            extras.putString("LIMIT", priceLimit)
            extras.putString("DEL", deleted)
            extras.putString("NAME", stuffname)
            return Intent().setClass(context, CostsActivity::class.java).putExtras(extras)
        }
    }

    private lateinit var binding: ActivityCostsBinding
    private lateinit var costsAdapter: CostsAdapter
    private lateinit var dateListId: String
    private var maxPrice: Double = 0.0
    private var amount: Double = 0.0
    private var deleted: Double = 0.0
    private var name: String = "Nothing yet"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return
        }

        dateListId = intent.getStringExtra("LIST_ID")!!

        binding = ActivityCostsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        amount = intent.getStringExtra("LIMIT")!!.toDouble()
        binding.etSum.text = amount.toString()
        deleted = intent.getStringExtra("DEL")!!.toDouble()
        name = intent.getStringExtra("NAME")!!

        costsAdapter = CostsAdapter(this)
        binding.rvCostList.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        binding.rvCostList.adapter = costsAdapter

        binding.floatingActionButton.setOnClickListener {
            CostDialogFragment(amount).show(supportFragmentManager, "NEW_COST")
        }

        binding.etButton.setOnClickListener{
            InfoDialogFragment(deleted, name).show(supportFragmentManager, "JUST_INFO")
        }

        createDateListListener()
        createCostsListener()
    }

    private fun createDateListListener() {
        DataBase.getInstance().addDateListsListener { snapshots, error ->
            if (error != null) {
                return@addDateListsListener
            }
            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.REMOVED -> {
                        if (dc.document.id.equals(dateListId)) {
                            showSnackbar(R.string.list_deleted_by_someone_else)
                            finish()
                            return@addDateListsListener
                        }
                    }
                }
            }
        }
    }

   private fun createCostsListener() {
       DataBase.getInstance().addCostListener(dateListId) { snapshots, error ->
           if (error != null) {
               Log.w(TAG, "Error at snapshot listener", error)
               return@addCostListener
           }

           for (dc in snapshots!!.documentChanges) {
               when (dc.type) {
                   DocumentChange.Type.ADDED -> {
                       costsAdapter.addCost(
                           dc.document.toObject<Cost>(),
                           dc.document.id
                       )
                   }
                   DocumentChange.Type.REMOVED -> {
                       costsAdapter.removeCost(dc.document.id)
                   }
               }
           }
       }
    }

    private fun showSnackbar(errorMessageRes: Int) {
        Snackbar.make(binding.root, getString(errorMessageRes), Snackbar.LENGTH_LONG).show()
    }

    override fun costCreated(cost: Cost, price : String) {
        if (maxPrice < price.toDouble()) {
            maxPrice = price.toDouble()
            name = cost.name.toString()
        }
        amount -= price.toDouble()
        deleted += price.toDouble()
        binding.etSum.text = amount.toString()
        DataBase.getInstance().createCost(dateListId, cost, amount.toString(), deleted.toString(), name)
        if (amount * 2 < deleted) {
            Snackbar.make(binding.root, "You almost spent all of your money", Snackbar.LENGTH_LONG).show()
        }
        else if(amount < deleted) {
                Snackbar.make(binding.root, "You spent half of your money", Snackbar.LENGTH_LONG).show()
        }
    }

    fun deleteCost(costId: String, price: Double?, temp: Cost, size: Int) {
        if(size == 1) {
            maxPrice = 0.0
            name = "Nothing yet"
        }
        else if (maxPrice == price!!) {
            maxPrice = temp.price!!
            name = temp.name.toString()
        }
        if (price != null) {
            amount += price
            deleted -= price
        }
        binding.etSum.text = amount.toString()
        DataBase.getInstance().deleteCost(dateListId, costId, amount.toString(), deleted.toString(), name)
    }
}