package hu.bme.aut.android.costaccountant.data

import java.io.Serializable
import kotlin.collections.HashMap

class Cost(
    var name: String? = null,
    var price: Double? = null,
    var category: Category? = null,
    var date: String? = null,
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "price" to price,
            "category" to category,
            "date" to date,
        )
    }

    enum class Category {
        DIESEL, FOOD, HOTEL, MEDICINE, ANOTHER;

        companion object {

            fun getByOrdinal(ordinal: Int): Category? {
                if (ordinal < 0 || ordinal >= values().size)
                    return null
                return values()[ordinal]
            }
        }
    }
}