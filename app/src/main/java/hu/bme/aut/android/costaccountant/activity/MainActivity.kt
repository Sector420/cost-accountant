package hu.bme.aut.android.costaccountant.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.util.ExtraConstants
import com.google.android.gms.tasks.Task
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.costaccountant.R
import hu.bme.aut.android.costaccountant.adapter.DateListsAdapter
import hu.bme.aut.android.costaccountant.data.DateList
import hu.bme.aut.android.costaccountant.databinding.ActivityMainBinding
import hu.bme.aut.android.costaccountant.db.DataBase
import hu.bme.aut.android.costaccountant.fragment.DateListDialogFragment
import hu.bme.aut.android.costaccountant.fragment.JoinDialogFragment


class MainActivity : AppCompatActivity(), DateListDialogFragment.DateListHandler, JoinDialogFragment.JoinHandler {

    companion object {
        private const val TAG = "MainActivity"

        fun createIntent(context: Context, response: IdpResponse?): Intent {
            return Intent().setClass(context, MainActivity::class.java)
                .putExtra(ExtraConstants.IDP_RESPONSE, response)
        }
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var dateListsAdapter: DateListsAdapter
    private lateinit var user: FirebaseUser

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return
        }

        user = currentUser!!

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.topAppBar)

        dateListsAdapter = DateListsAdapter(this)

        binding.rvDateLists.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        binding.rvDateLists.adapter = dateListsAdapter

        binding.fab.setOnClickListener {
            DateListDialogFragment().show(supportFragmentManager, "NEW_DATE")
        }

        createDateListListener()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_activity_toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.join_with_code -> {
                joinWithCode()
                true
            }
            R.id.sign_out -> {
                signOut()
                true
            }
            R.id.delete_account -> {
                deleteAccountClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun deleteAccountClicked() {
        MaterialAlertDialogBuilder(this)
            .setMessage("Are you sure you want to delete this account?")
            .setPositiveButton("Yes") { _, _ -> deleteAccount() }
            .setNegativeButton("No", null)
            .show()
    }

    private fun deleteAccount() {
        AuthUI.getInstance()
            .delete(this)
            .addOnCompleteListener(
                this
            ) { task: Task<Void?> ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@MainActivity))
                    finish()
                } else {
                    showSnackbar(R.string.delete_account_failed)
                }
            }
    }

    private fun signOut() {
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener { task: Task<Void?> ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@MainActivity))
                    finish()
                } else {
                    Log.w(
                        TAG,
                        "signOut:failure",
                        task.exception
                    )
                    showSnackbar(R.string.sign_out_failed)
                }
            }
    }

    private fun joinWithCode() {
        JoinDialogFragment().show(supportFragmentManager, "null")
    }

    override fun join(code: String) {
        DataBase.getInstance().joinDateList(code)
    }

    private fun createDateListListener() {
        DataBase.getInstance().addDateListsListener { snapshots, error ->
            if (error != null) {
                Log.w(TAG, "Error at snapshot listener", error)
                return@addDateListsListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        dateListsAdapter.addDate(
                            dc.document.toObject<DateList>(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        dateListsAdapter.updateDateList(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        dateListsAdapter.removeDate(dc.document.id)
                    }
                }
            }
        }
    }

    fun startCostsActivity(
        dateListId: String,
        priceLimit: String?,
        deleted: String?,
        stuffname: String
    ) {
        startActivity(CostsActivity.createIntent(this, dateListId, priceLimit, deleted, stuffname))
    }

    fun shareDateList(dateListId: String) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, dateListId)
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, "Share date list"))
    }

    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(binding.root, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

    override fun createDateList(name : String, price : String, start : String, end : String) {
        val del = "0.0"
        val stuffname = "Nothing yet"
        DataBase.getInstance().createDateList(DateList(name, price, del, start, end, stuffname, mutableListOf(user.uid)))
    }

    fun deleteDateList(dateListId: String) {
        DataBase.getInstance().deleteDateList(dateListId)
    }
}