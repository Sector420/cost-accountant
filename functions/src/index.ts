import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as Firestore from "@google-cloud/firestore";

admin.initializeApp();

export const joinToDateList = functions
  .region("europe-west3")
  .firestore.document("/joinRequests/{requestID}")
  .onCreate((snapshot, context) => {
    const data = snapshot.data();
    if (data) {
      const userUid = data.userUid;
      const dateListId = data.dateListId;
      let p1 = snapshot.ref.delete();
      let p2 = admin
        .firestore()
        .doc(`lists/${dateListId}`)
        .update("theboys", Firestore.FieldValue.arrayUnion(userUid));
      return Promise.all([p1, p2]);
    } else {
      return null;
    }
  });